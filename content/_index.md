+++
title = "Teaching and Learning Computer Science"
date = 2018-08-24T10:53:05-05:00
archetype = "home"
+++

This is a free, online textbook for anyone interested in teaching Computer Science at any level.  For those who do not come from a computer science background, it provides a brief exploration of the field, its history, and foundations. It also introduces the current understanding of how people learn computer science emerging from philosophy of education and cognitive science. Then it covers teaching approaches, lesson design strategies, and useful tools for teachers to adopt in their efforts.

The text was created for Kansas State University's teacher training programs, and is released under the [Creative Commons BY-NC-SA 4.0 License](https://creativecommons.org/licenses/by-nc-sa/4.0/).