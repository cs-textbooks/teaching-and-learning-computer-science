---
title: "CS Unplugged"
pre: "2. "
weight: 20
date: 2018-08-24T10:53:26-05:00
---

Learning computer science can be _hard_ - you're trying to learn the skills of computational thinking _and_ a new language at the same time.  Consider what it would be like learning algebra for the first time in _Spanish_ at the same time you were learning Spanish, and you'll get the idea of the challenge. 

Of course, there are important differences: 
1. With a language like Spanish, a skilled speaker will be able to interpret your meaning even if you don't quite follow the grammatical rules of the language.  A computer will not - you must follow the rules exactly!
2. A programming language contains a much smaller vocabulary and fewer grammatical rules than a human one like Spanish, so there is ultimately less to learn.
3. Programming languages typically use English words, so you don't have to learn a whole new set of words and meanings... except the way a programming language _uses_ those words doesn't always line up with our expectations based on the English language - this makes for some significant **gotchas**. 

[CS Unplugged](https://www.csunplugged.org/en/) curriculum is an attempt to sidestep the programming language challenge entirely, and focus on teaching computational thinking _with no computers at all_. Instead, it teaches the concepts through games, puzzles, and lots of physical real-world action.

Based on our readings from chapter 2, you probably realize that we _need_ to incorporate use of an actual computer and programming language to help our students develop an accurate _internal notional machine_ to guide problem-solving efforts to utilize a computer.  And indeed, research into CS Unplugged suggests that by itself, it does not help students become better programmers.  This is actually part of a broader issue of _knowledge transfer_ - humans are not great at taking what they learn in one domain and applying it in another, without guidance on how the two connect.

But combining CS Unplugged activities with the other strategies laid out in this chapter can be a recipe for success. When you do so, be sure to explicitly call out the connections between the various activities to help your students see how it all connects.