---
title: "Coaching"
pre: "8. "
weight: 80
date: 2018-08-24T10:53:26-05:00
---

As students gain knowledge and skills in programming, we want to provide less structured scaffolding and allow them to develop more independent problem-solving skills.  However, it is still common for a student to encounter an issue that is beyond their ability to solve even after they have developed significant skills (in fact, expert programmers _still_ often find themselves confronted with a challenge they aren't sure how to tackle). If students are left to struggle too long, frustration sets in, and learning is derailed.

Thus, as we move towards more independent programming, we want to shift to the role of a coach or mentor, and watch for those high levels of frustration and step in to provide guidance.  It is important not to step in too early - as we can unwittingly relieve the cognitive dissonance the students need to be encountering to learn. But it is also important not to let them struggle too long with a problem beyond their current understanding.

Also, when coaching, be very careful not to 'solve' the issue for your students. Keep your hands off their keyboard and mouse!  Ask them leading questions to help them find the answer themselves, and have them add it to their programs themselves.

Peer coaching - allowing students to help each other - can also be a powerful tool. In peer coaching, more experienced students coach less-experienced ones (Note this is different from peer programming, in which the students are at a similar level of proficiency). However, peer coaching should be closely monitored, as bad interactions can discourage learners and dampen interest in computer science entirely.