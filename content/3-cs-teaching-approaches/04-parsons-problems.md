---
title: "Parsons Problems"
pre: "4. "
weight: 40
date: 2018-08-24T10:53:26-05:00
---

[Parsons problems](https://en.wikipedia.org/wiki/Parsons_problems) are another commonly used tool for reducing the cognitive load of learning to program. A Parsons Problem gives the student all the lines of code needed to complete a task, and asks them to arrange them in order. While this can be done on paper (by marking an order), they are commonly delivered using a drag-and-drop interactive interface that also allows the student to verify they have completed the task correctly:

![Parsons Problem Example](/images/3.3.1.gif)

Parsons problems can be used with _any_ programming language, including text-based ones like the Python example above. But they offer some of the same benefits of block-based programming: the student doesn't need to remember the exact syntax constructions - just _recognize_ them and be able to order them correctly.

Thus, Parsons problems are commonly used to practice syntax as students are learning it. Additionally, they are commonly used in assessments. You can fine-tune the challenge of a Parsons problem by "fixing" some of the solution lines in place (so they aren't draggable, and by adding detractors (lines that don't actually belong in the program). 