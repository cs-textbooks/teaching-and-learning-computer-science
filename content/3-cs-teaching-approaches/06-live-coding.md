---
title: "Live Coding"
pre: "6. "
weight: 60
date: 2018-08-24T10:53:26-05:00
---

An approach that has been used to teach computer science for many years is _Live Coding_ (also known as _Demonstration Coding_ or _Apprenticeship Coding_). In this strategy, the teacher models the activities of solving a problem and writing the corresponding program in front of the class.  

An important aspect of this approach is explaining your thought process as you approach, think about, solve, the problem and write the program solution. By exposing your thought process out loud, you are guiding your students to think about their own processes.

It is a good idea to work through the problem and solution you want to demonstrate several times before you present it to students - but be sure to keep notes on your thought process during your first attempts so you can present it accurately.  You don't want to present students with a perfect step-by-step process; doing so will give them an unrealistic expectation of what programming should be.  Showing them how you encounter and debug errors will help them understand that it is a normal part of the programming process.  But you also don't want so many that it derails the momentum of the lesson.

Live coding can be used with any part of working in CS, and combined with many of the pedagogical approaches we will discuss shortly. 

Finally, some teachers have students "follow along" - write the same program the teacher is demonstrating.  This can be helpful for getting students more practice programming and keep students' attention focused on the lesson. But it also adds additional cognitive load, and may result in students missing some of the benefits of your modeling as they focus on getting their program to match yours. Also, you may find that students want you to move at different speeds - some want to go fast, while others will need you to slow down to get everything written. For large classes, this can become a major challenge.  A good alternative for follow-along live coding is pre-recording a programming session and providing it as a video for your students to use. This allows them to proceed at their own pace, and also go back to review parts they missed.