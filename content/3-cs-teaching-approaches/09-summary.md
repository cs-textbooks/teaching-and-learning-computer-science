---
title: "Summary"
pre: "9. "
weight: 90
date: 2018-08-24T10:53:26-05:00
---

In this chapter we explored tools and techniques for teaching computer science and limiting cognitive load. These include approaches that simplify the use of programming from eliminating it altogether (CS Unplugged) to providing a drag-and-drop interface to remove the need to type syntax correctly (block-based programming and Parsons problems).

We also discussed the importance of reading code for comprehension, which helps to develop the student's internalized notional machine for the programming language they are learning. Likewise, live coding - modeling our development practice for our students can help them develop the skills necessary to write their own programs. Finally, peer programming and coaching can help support students as they continue to develop their own proficiency.