---
title: "Pair Programming"
pre: "7. "
weight: 70
date: 2018-08-24T10:53:26-05:00
---

Peer programming is a technique where two students work together to create a program solution. One student takes on the role of the _driver_, and writes the actual program code, while the second is the _navigator_, who provides guidance on what code needs to be written. In effect we are reducing the cognitive load in that each student only needs to tackle a subset of the challenges in writing software. The navigator is primarily employing the skills of computational thinking, while the driver is primarily engaging in programming.

This approach also means two pairs of eyes (and the minds behind them) are seeing the code as it is written, which can help increase the chances that simple mistakes (like typos, using the wrong variable name, etc.) will be caught early. It also encourages the two programmers to discuss strategy and plan rather than jumping into programming blindly.  For many of these reasons, pair programming is also used in professional practice.