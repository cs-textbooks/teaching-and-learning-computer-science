---
title: "Introduction"
pre: "1. "
weight: 10
date: 2018-08-24T10:53:26-05:00
---

Now that you understand more about what computer science is, and how we learn it, we can turn our attention to pedagogy and tools for effectively _teaching_ computer science. Remember, our goal is to help students develop cognitive structures that help them **read and write programs** and **solve problems with computational thinking**. Ultimately, there is a _lot_ for students to learn in order to engage with computer science effectively. We therefore need to carefully scaffold our teaching efforts to help minimize the cognitive load our students encounter as they develop the mental structures they need to reason about both programming and computational thinking. We need to keep the students in the zone (the zone of proximal development)!

This chapter will discuss tools and teaching techniques to help manage this cognitive load.