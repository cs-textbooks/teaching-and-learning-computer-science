---
title: "Draft Syllabus"
weight: 30
pre: ""
---

{{% notice info %}}
This course is intended for PreK-12 educators.  
{{% /notice %}} 

## CC 750 - Data Structures and Algorithms for Educators I

3 Credit Hours

### Prerequisites

* B or better in MATH 100 - College Algebra
* B or better in CC 730 - Computer Programming for Educators

### Course Overview

Exploration of data structures & related algorithms in computer programming. Basic concepts of complexity analysis. Object-oriented design concepts. Pedagogical strategies for teaching data structures and algorithms. 

### Course Description

This course provides PreK-12 educators the content knowledge necessary for teaching simple data structures such as sets, lists, stacks, queues, and maps. Students learn how to create data structures and the algorithms to work with them. Students are introduced to algorithm analysis to determine the efficiency of algorithms. It also addresses strategies for teaching these concepts to a diverse audience.

### Major Course Topics

* Data Structures
  * Sets
  * Lists
  * Stacks
  * Queues
  * Maps
* Algorithms
  * Searching
  * Sorting
  * Structural Operations
  * Hashing
  * Set Relations
* Recursion
* Complexity Analysis
* Algorithm Design Strategies and Patterns
* Logic: Preconditions, Postconditions and Invariants
* Pedagogical techniques for teaching Data Structures

###  Course Structure

These courses are being taught 100% online, and each module is self-paced. There may be some bumps in the road as we refine the overall course structure. Students will work at their own pace through a set of modules, with approximately one module being due each week. Material will be provided in the form of recorded videos, online tutorials, links to online resources, and discussion prompts. Each module will include a coding project or assignment, many of which will be graded automatically through Codio. Assignments may also include portions which will be graded manually via Canvas or other tools.

#### Reflective Journal
A second task will be to reflect on what you are learning throughout your course journey, especially in how it impacts you and your teaching. Each module will include a reflective journal entry assignment. 

### Grading

In theory, each student begins the course with an A. As you submit work, you can either maintain your A (for good work) or chip away at it (for less adequate or incomplete work). In practice, each student starts with 0 points in the gradebook and works upward toward a final point total earned out of the possible number of points. In this course, each assignment constitutes a portion of the final grade, as detailed below:

* 60% - Codio Programming Projects
* 10% - Codio Tutorials and Canvas Quizzes
* 10% - Reflective Journal
* 20% - Final Exam

Up to 5% of the total grade in the class is available as extra credit. See the **Extra Credit - Bug Bounty** assignment for details.

Letter grades will be assigned following the standard scale:

* 90% - 100% &rarr; A
* 80% - 89.99% &rarr; B
* 70% - 79.99% &rarr; C
* 60% - 69.99% &rarr; D
* 00% - 59.99% &rarr; F

### Recommended Texts & Supplies

To participate in this course, students must have access to a modern web browser and broadband internet connection. All course materials will be provided via Canvas and Codio. Modules may also contain links to external resources for additional information, such as programming language documentation.
