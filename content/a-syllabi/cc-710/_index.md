---
title: "CC 710 - Introduction to Computing for Educators"
weight: 10
pre: ""
---

{{% notice info %}}

This course is intended for PreK-12 educators participating in the state PACK grant or students who are seeking a graduate degree or certificate in education.

{{% /notice %}}

## CC 710 - Introduction to Computing for Educators (2023-2024)

[Previous Versions]({{<relref "./old">}})

2 Credit Hours

### Prerequisites

-   None

### Instructor Contact Information

{{% notice info %}}

**All emails** for the course should be sent to [cc710help@ksuemailprod.onmicrosoft.com](mailto:cc710help@ksuemailprod.onmicrosoft.com?subject=CC%20710%20Help) (sorry, it’s a long address). This will contact the instructors and ALL the TAs for the course and guarantee the fastest response time if contacting via email (please allow one full business day for response). You are welcome to send emails that may contain more sensitive information directly to intended recipients.

Communication can also be done through Ed Discussion and other forms listed [down below](#How-to-Get-Help-in-this-Course).

{{% /notice %}}

**Professor:** Josh Weese (he/him) – [weeser@ksu.edu](mailto:weeser@ksu.edu)

-   **Office:** 2214 Engineering Hall (DUE)
-   **Office Hours:** See office hours calendar below or by appointment via Zoom
-   **Want to meet with me outside office hours?** [https://calendly.com/weeser](https://calendly.com/weeser)

**Instructor:** Russell Feldhausen – [russfeld@ksu.edu](mailto:russfeld@ksu.edu)

-   **Office**: DUE 2213, but I mostly work remotely from Kansas City
-   **Phone**: (785) 292-3121 (Call/Text)
-   **Website**: [https://russfeld.me](https://russfeld.me)
-   **Office Hours**: See office hours calendar below or by appointment via Zoom
-   **Want to meet with me outside office hours?** [https://calendly.com/russfeld](https://calendly.com/russfeld)

**Professor:** Nathan Bean (he/him) – [nhbean@ksu.edu](mailto:nhbean@ksu.edu)

-   **Office:** 2216 Engineering Hall (DUE)
-   **Website:** [https://nathanhbean.com/](https://nathanhbean.com/)
-   **Office Hours:** See office hours calendar below or by appointment via Zoom

**Teaching Assistants**
_All TA office hours can be found on the embedded calendar below._

- Sumaira Ghazal (GTA)
- Friday James (GTA)
- Pavan Kumar Reddy Lakkireddy (GTA)
- Yihong Theis (GTA)
- Timothy Tucker (GTA)
- Carrie Aponte
- Josh Barron
- Supriya Bolla
- Cole James
- Ryan Stueve

<iframe style="border: solid 1px #777;" src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FChicago&amp;mode=WEEK&amp;showTitle=0&amp;src=MWZmcTd1N2RkM2Z0dDFycms2N2YzcHY4cGdAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=ZW4udXNhI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23616161&amp;color=%23009688" width="800" height="600"></iframe>

### Course Overview

An overview of the history of computers and programming. Famous historical figures and their impact in modern computing. Introduction to major topics in computer science such as artificial intelligence, high performance computing, cryptography, big data, cyber security, robotics, and more. The course also includes an exploration of computational thinking and learning theories as well.

### Course Description

This course is a first step in preparing educators to teach computer science topics at the PreK-12 level. It provides content area knowledge including the historical context in which computing developed, an overview of the modern field of computing and its impacts on society. It also explores theories of computational thinking and learning, computer science education research, and the Kansas PreK-12 computer science standards.

### Required Texts

To participate in this course, students must have access to a modern web browser and broadband internet connection. Course materials will be provided via Canvas and Codio. Modules may also contain links to external resources for additional information, such as programming language documentation. There are also some required textbooks (some are free) listed below:

-   “The Pattern on the Stone: The Simple Ideas that Make Computers Work” by W. Daniel Hillis. **ISBN 046502596X**, newer version is also available and will work fine
    -   Free digital version though the KSU Library: https://k-state.primo.exlibrisgroup.com/permalink/01KSU_INST/1bihi0g/alma9942605917302401
-   “Nine Algorithms That Changed the Future: The Ingenious Ideas That Drive Today’s Computers” by John MacCormick. **ISBN 0691158193**

### Optional Reading

These are books we’ve found to be interesting and/or useful:

-   “Tubes: A Journey to the Center of the Internet” by Andrew Blum. **ISBN 0061994952**
-   “Blown to Bits: Your Life, Liberty, and Happiness After the Digital Explosion” by Hal Abelson, Ken Ledeen, and Harry Lewis. **ISBN 013713559,** Creative Commons digital edition available **FREE** at http://www.bitsbook.com/
-   “Code: The Hidden Language of Computer Hardware and Software” by Charles Petzold. **ISBN 0735611319**
-   “How Not to be Wrong: The Power of Mathematical Thinking” by Jordan Ellenberg. **ISBN 0143127535**
-   “The Innovators: How a Group of Hackers, Geniuses and Geeks Created the Digital Revolution” by Walter Isaacson. **ISBN 1476708703**

### Learning Objectives

By the end of this course, each student will be able to:

-   Describe the history of Computing Science and list some of the important devices, innovations, and people that got us to where we are today.
-   Relate Computing Science to a variety of other disciplines and describe how they are interconnected with each other.
-   Apply and use Computing Science tools and techniques to solve real-world problems.
-   Research and learn about new ideas and areas in Computing Science and share those ideas with others.
-   Develop a personal understanding of how Computing Science affects his or her own life.
-   Understand many different subject areas within Computing Science and how they are changing our understanding of the field.
-   Describe how current theories of learning apply to learning Computer Science and programming
-   Be able to articulate current findings in Computer Science Education Research
-   Describe the critical issues facing the field of computer science (and how these can be addressed by educators)
    -   Lack of diversity in the field
    -   Ethical and cybersecurity concerns

### Major Course Topics

-   The history of computer science and early computing machines
-   The basics of binary representation, Boolean logic, data encoding, encryption, and error checking
-   Computational thinking, programming, and algorithm design
-   The history and technology behind the internet and how it affects our world
-   Computer science areas such as artificial intelligence, human-computer interaction, high performance computing, data science, robotics, and more
-   Cybersecurity in a modern, interconnected world
-   Other disciplines related to computer science
-   Experience with basic programming concepts
-   Theories of Learning Programming

### Course Structure

These courses are being taught 100% online, and each module is self-paced. There may be some bumps in the road as we refine the overall course structure. Students will work at their own pace through a set of modules, with approximately one module being due each week. Material will be provided in the form of recorded videos, online tutorials, links to online resources, and discussion prompts. Assignments may also include portions which will be graded manually via Canvas or other tools.

### Assignments

{{% notice warning %}}

Assignments completed and submitted as part of the course should be **your own work** unless otherwise stated. It is crucial that all work you do is your own. Posting (even if you don't get a response) course content on Stack Overflow, Chegg, or other similar websites is expressly forbidden and will result in an XF. This also includes viewing solutions to course content that has not been provided to you through canvas by your instructor or TA. The use of AI assisted tools to write your assignments is also explicitly forbidden (ChatGPT, GitHub Code Pilot, etc.). You may use these tools as help in the learning process, but work that you submit for a grade must be 100% done by you and only you.

_If you are struggling in the course or you have doubts on something, please ask! Your instructors and TAs are here to help!_

{{% /notice %}}

There will be some programming or written assignments given from time to time that must be completed outside of class. It is acceptable to communicate with other students about the concepts in the assignment if you do not understand it, but you should not discuss the details of how the assignment should be completed. Your submission should be your own work, or the work of your small group if allowed by the instructor. Some work will require you to look up certain programming syntax and techniques on the internet, **but** there is a fine line between learning and copying! **_When in doubt, ask!_**

#### Reflective Journal

A second task will be to reflect on what you are learning throughout your course journey, especially in how it impacts you and your teaching Each module will include a reflective journal entry assignment.

### Late Work

This course is being offered semi-synchronously. We understand the majority of you taking this course are working professionals. Content within modules will be asynchronous; however, each module will have a due date. That being said, we know that life/work can get in the way of things and are already aware of some folks who will need an adjusted schedule for the course content. Please let us know and we are very happy to create a modified schedule for those who need it.

### Grading

In theory, each student begins the course with an A. As you submit work, you can either maintain your A (for good work) or chip away at it (for less adequate or incomplete work). In practice, each student starts with 0 points in the gradebook and works upward toward a final point total earned out of the possible number of points. In this course, each assignment constitutes a portion of the final grade, as detailed below:

-   60% Canvas Quizzes (_Lowest 3 scores dropped_)
-   20% Reflective Journals
-   20% Discussions

Letter grades will be assigned following the standard scale:

-   90% - 100% &rarr; A
-   80% - 89.99% &rarr; B
-   70% - 79.99% &rarr; C
-   60% - 69.99% &rarr; D
-   00% - 59.99% &rarr; F

### How to Get Help in this Course

This really is an interesting course due to the large amount of material covered and much of the material is brand new to students. It is designed with zero-knowledge in mind, so if you are new or even experienced, you are in the right place! Everyone is encouraged to seek help whenever you feel you are being overwhelmed or don’t understand a topic. **You are not alone!** Most students have never studied anything relating to computer science before, so it is new to everyone. The professors and TAs are **_always_** willing to help students with **_any_** questions you may have about the class or other issues related to Computing Science. So please, don’t be afraid to ask questions. Get help early and often!

Here are the **recommended ways to get help**:

-   Review the course materials posted on K-State Canvas, the course website, and Codio
-   Post a question in the **Ed Discussion Forum**. This is found through the Canvas course navigation menu. _Note that you may need to click the navigation link in Canvas if this is your first time using Ed Discussion for this course!_
-   Visit office hours
-   Send questions to the course help email [cc710help@KSUemailProd.onmicrosoft.com](mailto:cc710help@KSUemailProd.onmicrosoft.com?subject=CC%20710%20Help)
-   Ask your classmates for help or advice on assignments or projects (be mindful of the honor code!)
-   Schedule a one-on-one meeting with your professor/TA

## Safe Zone Statement

We are part of the SafeZone community network of trained K-State faculty/staff/students who are available to listen and support you. As a SafeZone Ally, we can help you connect with resources on campus to address problems you face that interfere with your academic success, particularly issues of sexual violence, hateful acts, or concerns faced by individuals due to sexual orientation/gender identity. Our goal is to help you be successful and to maintain a safe and equitable campus.

{{< syllabus exclude="copyright" >}}

## Subject to Change

The details in this syllabus are not set in stone. Due to the flexible nature of this class, adjustments may need to be made as the semester progresses, though they will be kept to a minimum. If any changes occur, the changes will be posted on the Canvas page for this course and emailed to all students.

## Copyright Notification

Copyright 2023 (Joshua L. Weese, Nathan H. Bean, and Russell Feldhausen) as to this syllabus and lectures/content not licensed under our open source initiative. During this course students are prohibited from selling notes to or being paid for taking notes by any person or commercial firm without the express written permission of the professor teaching this course. In addition, students in this class are not authorized to provide class notes or other class-related materials to any other person or entity, other than sharing them directly with another student taking the class for purposes of
