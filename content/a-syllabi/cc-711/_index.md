---
title: "CC 711 - Computer Education Programming Fundamentals"
weight: 11
pre: ""
---

{{% notice info %}}

This course is intended for PreK-12 educators participating in the state PACK grant or students who are seeking a graduate degree or certificate in education.

{{% /notice %}}

## CC 711 - Computer Education Programming Fundamentals (2023-2024)

[Previous Versions]({{<relref "./old">}})

1 Credit Hour

### Prerequisites

-   CC 710 - Introduction to Computing for Educators (Prerequisite or Concurrent Enrollment)

### Instructor Contact Information

{{% notice info %}}

**All emails** for the course should be sent to [cc710help@ksuemailprod.onmicrosoft.com](mailto:cc710help@ksuemailprod.onmicrosoft.com?subject=CC%20710%20Help) (sorry, it’s a long address). This will contact the instructors and ALL the TAs for the course and guarantee the fastest response time if contacting via email (please allow one full business day for response). You are welcome to send emails that may contain more sensitive information directly to intended recipients.

Communication can also be done through Ed Discussion and other forms listed [down below](#How-to-Get-Help-in-this-Course).

{{% /notice %}}

**Professor:** Josh Weese (he/him) – [weeser@ksu.edu](mailto:weeser@ksu.edu)

-   **Office:** 2214 Engineering Hall (DUE)
-   **Office Hours:** See office hours calendar below or by appointment via Zoom
-   **Want to meet with me outside office hours?** [https://calendly.com/weeser](https://calendly.com/weeser)

**Instructor:** Russell Feldhausen – [russfeld@ksu.edu](mailto:russfeld@ksu.edu)

-   **Office**: DUE 2213, but I mostly work remotely from Kansas City
-   **Phone**: (785) 292-3121 (Call/Text)
-   **Website**: [https://russfeld.me](https://russfeld.me)
-   **Office Hours**: See office hours calendar below or by appointment via Zoom
-   **Want to meet with me outside office hours?** [https://calendly.com/russfeld](https://calendly.com/russfeld)

**Professor:** Nathan Bean (he/him) – [nhbean@ksu.edu](mailto:nhbean@ksu.edu)

-   **Office:** 2216 Engineering Hall (DUE)
-   **Website:** [https://nathanhbean.com/](https://nathanhbean.com/)
-   **Office Hours:** See office hours calendar below or by appointment via Zoom

**Teaching Assistants**
_All TA office hours can be found on the embedded calendar below._

- Sumaira Ghazal (GTA)
- Friday James (GTA)
- Pavan Kumar Reddy Lakkireddy (GTA)
- Yihong Theis (GTA)
- Timothy Tucker (GTA)
- Carrie Aponte
- Josh Barron
- Supriya Bolla
- Cole James
- Ryan Stueve

<iframe style="border: solid 1px #777;" src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FChicago&amp;mode=WEEK&amp;showTitle=0&amp;src=MWZmcTd1N2RkM2Z0dDFycms2N2YzcHY4cGdAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=ZW4udXNhI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23616161&amp;color=%23009688" width="800" height="600"></iframe>

### Course Overview

Brief experience with computer programming concepts such as variables, data types, functions, conditionals, iteration, and collections, with a focus on pedagogical techniques for teaching these concepts effectively to a diverse audience.

### Course Description

This course introduces PreK-12 educators to the "Big ideas" of programming - concept knowledge that is critical for developing and teaching programming skills. Students will learn to read simple program code, write short programs, and explore basic programming constructs such as conditional statements and iteration. Students will also explore basic programming concepts related to a particular discipline.

### Required Texts

To participate in this course, students must have access to a modern web browser and broadband internet connection. Course materials will be provided via Canvas and Codio. Modules may also contain links to external resources for additional information, such as programming language documentation. There are also some required textbooks (some are free) listed below:

-   “A Byte of Python” by Swwaroop C.H.
    -   This book is free! Its accessed at https://python.swaroopch.com/
    -   Download: https://github.com/swaroopch/byte-of-python/releases/latest

### Required Software

We will also be using Python 3 as the language for the programming assignments. It can be found at [python.org](https://www.python.org/). Information on downloading and installing Python will be given in class. All of the programming assignments, labs, etc. will be done through the Codio environment.

### Learning Objectives

By the end of this course, each student will be able to:

-   Describe the basic syntax of Python
-   Create programs in Python that use:
    -   variables and math operators to store and manipulate data
    -   functions, parameters, and returns to decompose programs into smaller parts
    -   conditional statements and iteration to change a program's control flow
    -   user inputs and print statements to interact with the user
    -   basic data structure such as lists and dictionaries
-   Relate their own experiences in learning programming to the learning theories discussed in CC 710

### Major Course Topics

-   Introduction to Programming in Python
-   Basic Language Syntax
-   Variables
-   Math Operators
-   User Input
-   Functions, Parameters, Returns
-   Booleans and Boolean Logic
-   Conditional Statements
-   Iteration
-   Nesting Conditional and Iterative Statements
-   Lists and Dictionaries

### Course Structure

These courses are being taught 100% online, and each module is self-paced. There may be some bumps in the road as we refine the overall course structure. Students will work at their own pace through a set of modules, with approximately one module being due each week. Material will be provided in the form of recorded videos, online tutorials, links to online resources, and interactive lab assignments. Assignments may also include portions which will be graded automatically in Canvas or manually via Codio, Canvas or other tools.

### Assignments

{{% notice warning %}}

Assignments completed and submitted as part of the course should be **your own work** unless otherwise stated. It is crucial that all work you do is your own. Posting (even if you don't get a response) course content on Stack Overflow, Chegg, or other similar websites is expressly forbidden and will result in an XF. This also includes viewing solutions to course content that has not been provided to you through canvas by your instructor or TA. The use of AI assisted tools to write your assignments is also explicitly forbidden (ChatGPT, GitHub Code Pilot, etc.). You may use these tools as help in the learning process, but work that you submit for a grade must be 100% done by you and only you.

_If you are struggling in the course or you have doubts on something, please ask! Your instructors and TAs are here to help!_

{{% /notice %}}

There will be some programming or written assignments given from time to time that must be completed outside of class. It is acceptable to communicate with other students about the concepts in the assignment if you do not understand it, but you should not discuss the details of how the assignment should be completed. Your submission should be your own work, or the work of your small group if allowed by the instructor. Some work will require you to look up certain programming syntax and techniques on the internet, **but** there is a fine line between learning and copying! **_When in doubt, ask!_**

#### Lesson Plans

Throughout the course, you will be asked to apply what you've learned and your experience in education to develop lesson plans to teach basic programming concepts to students in your courses.

#### Programming Work

Finally, this course will include many programming labs and homeworks that are designed to give you some basic training and experience with computer programming in Python, as well as reasoning about programs using a mental model of a computer, or _notional machine_.

There is no shortcut to becoming a great programmer. Only by **doing the work** will you develop the skills and knowledge to make you a successful computer scientist. This course is built around that principle, and gives you ample opportunity to do the work, with as much support as we can offer.

**Programming Labs:** Each programming concept will first be introduced in one or more dedicated programming labs. These labs include video and text explanations of the topic, many worked examples, and short quizzes to help you check your understanding of concepts, comprehension of existing code, and ability to develop new code.

**Programming Homeworks:** Throughout the course, there will be several programming homeworks that allow you to practice your skills by developing short programs to meet a given specification.

**Programming Quizzes & Exams:** The course may also include a few programming quizzes or exams that are designed to assess your understanding of programming concepts without the aid of other tools or online resources.

{{% notice warning%}}

Programming assignments that are turned in and do not run or that are not in the required format will receive a grade of 0, **no exceptions**.

{{% /notice %}}

### Late Work

This course is being offered semi-synchronously. We understand the majority of you taking this course are working professionals. Content within modules will be asynchronous; however, each module will have a due date. That being said, we know that life/work can get in the way of things and are already aware of some folks who will need an adjusted schedule for the course content. Please let us know and we are very happy to create a modified schedule for those who need it.

### Grading

In theory, each student begins the course with an A. As you submit work, you can either maintain your A (for good work) or chip away at it (for less adequate or incomplete work). In practice, each student starts with 0 points in the gradebook and works upward toward a final point total earned out of the possible number of points. In this course, each assignment constitutes a portion of the final grade, as detailed below:

-   50% Programming Labs (_Lowest score dropped_)
-   30% Programming Homeworks
-   10% Lesson Plans
-   10% Exams

Letter grades will be assigned following the standard scale:

-   90% - 100% &rarr; A
-   80% - 89.99% &rarr; B
-   70% - 79.99% &rarr; C
-   60% - 69.99% &rarr; D
-   00% - 59.99% &rarr; F

### How to Get Help in this Course

This really is an interesting course due to the large amount of material covered and much of the material is brand new to students. It is designed with zero-knowledge in mind, so if you are new or even experienced, you are in the right place! Everyone is encouraged to seek help whenever you feel you are being overwhelmed or don’t understand a topic. **You are not alone!** Most students have never studied anything relating to computer science before, so it is new to everyone. The professors and TAs are **_always_** willing to help students with **_any_** questions you may have about the class or other issues related to Computing Science. So please, don’t be afraid to ask questions. Get help early and often!

Here are the **recommended ways to get help**:

-   Review the course materials posted on K-State Canvas, the course website, and Codio
-   Post a question in the **Ed Discussion Forum**. This is found through the Canvas course navigation menu. _Note that you may need to click the navigation link in Canvas if this is your first time using Ed Discussion for this course!_
-   Visit office hours
-   Send questions to the course help email [cc710help@KSUemailProd.onmicrosoft.com](mailto:cc710help@KSUemailProd.onmicrosoft.com?subject=CC%20710%20Help)
-   Ask your classmates for help or advice on assignments or projects (be mindful of the honor code!)
-   Schedule a one-on-one meeting with your professor/TA

## Safe Zone Statement

We are part of the SafeZone community network of trained K-State faculty/staff/students who are available to listen and support you. As a SafeZone Ally, we can help you connect with resources on campus to address problems you face that interfere with your academic success, particularly issues of sexual violence, hateful acts, or concerns faced by individuals due to sexual orientation/gender identity. Our goal is to help you be successful and to maintain a safe and equitable campus.

{{< syllabus exclude="copyright" >}}

## Subject to Change

The details in this syllabus are not set in stone. Due to the flexible nature of this class, adjustments may need to be made as the semester progresses, though they will be kept to a minimum. If any changes occur, the changes will be posted on the Canvas page for this course and emailed to all students.

## Copyright Notification

Copyright 2023 (Joshua L. Weese, Nathan H. Bean, and Russell Feldhausen) as to this syllabus and lectures/content not licensed under our open source initiative. During this course students are prohibited from selling notes to or being paid for taking notes by any person or commercial firm without the express written permission of the professor teaching this course. In addition, students in this class are not authorized to provide class notes or other class-related materials to any other person or entity, other than sharing them directly with another student taking the class for purposes of
