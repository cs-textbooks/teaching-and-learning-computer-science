---
title: "Draft Syllabus"
weight: 11
pre: ""
---

{{% notice info %}}
This course is intended for PreK-12 educators.  
{{% /notice %}} 

## CC 711 - Computer Education Programming Fundamentals

1 Credit Hour

### Prerequisites

* CC 710 - Introduction to Computing for Educators (Prerequisite or Concurrent Enrollment)

### Course Overview

Brief experience with computer programming concepts such as variables, data types, functions, conditionals, iteration, and collections, with a focus on pedagogical techniques for teaching these concepts effectively to a diverse audience.

### Course Description

This course introduces PreK-12 educators to the "Big ideas" of programming - concept knowledge that is critical for developing and teaching programming skills. Students will learn to read simple program code, write short programs, and explore basic programming constructs such as conditional statements and iteration. Students will also explore basic programming concepts related to a particular discipline.

### Learning Objectives

By the end of this course, each student will be able to:
* Describe the basic syntax of both Python and Pseudocode
* Create programs in Python and Pseudocode that use: 
   * variables and math operators to store and manipulate data
   * functions, parameters, and returns to decompose programs into smaller parts
   * conditional statements and iteration to change a program's control flow
   * user inputs and print statements to interact with the user
   * basic data structure such as lists and dictionaries
* Create a simple website using HTML, CSS and JavaScript
* Relate your own experiences in learning programming to the learning theories discussed in CC 710

### Major Course Topics

* Introduction to Programming in Pseudocode and Python 
* Basic Language Syntax 
* Variables 
* Math Operators 
* User Input 
* Functions, Parameters, Returns 
* Booleans and Boolean Logic 
* Conditional Statements 
* Iteration 
* Nesting Conditional and Iterative Statements 
* Lists and Dictionaries 
* Basic Website Programming 

###  Course Structure

These courses are being taught 100% online, and each module is self-paced. There may be some bumps in the road as we refine the overall course structure. Students will work at their own pace through a set of modules, with approximately one module being due each week. Material will be provided in the form of recorded videos, online tutorials, links to online resources, and discussion prompts. Some modules will include a coding project or assignment, many of which will be graded automatically through Codio. Assignments may also include portions which will be graded manually via Canvas or other tools.

### Grading

In theory, each student begins the course with an A. As you submit work, you can either maintain your A (for good work) or chip away at it (for less adequate or incomplete work). In practice, each student starts with 0 points in the gradebook and works upward toward a final point total earned out of the possible number of points. In this course, each assignment constitutes a portion of the final grade, as detailed below:

* 40% Programming Labs
* 40% Programming Homeworks
* 20% Final Exam

Letter grades will be assigned following the standard scale:

* 90% - 100% &rarr; A
* 80% - 89.99% &rarr; B
* 70% - 79.99% &rarr; C
* 60% - 69.99% &rarr; D
* 00% - 59.99% &rarr; F

### Recommended Texts & Supplies

To participate in this course, students must have access to a modern web browser and broadband internet connection. All course materials will be provided via Canvas and Codio. Modules may also contain links to external resources for additional information, such as programming language documentation. 
