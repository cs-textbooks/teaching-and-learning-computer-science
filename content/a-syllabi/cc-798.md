---
title: "CC 798 - Topics in Computing for Educators"
weight: 60
pre: ""
---

{{% notice info %}}
This course is intended for PreK-12 educators.  
{{% /notice %}} 

## CC 798 - Topics in Computing for Educators

1-18 Credit Hours

### Prerequisites

* Varies based on topic

### Course Overview

Topics in Computing Education for PreK-12 educators. Topics will be determined by faculty. 
