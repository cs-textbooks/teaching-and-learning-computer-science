---
title: "PRIMM"
pre: "7. "
weight: 70
date: 2018-08-24T10:53:26-05:00
---

PRIMM is also an instructional approach building off the Use &rarr; Modify &rarr; Create strategy and the Block Model.  Much like TIPP & SEE, it emphasizes reading and understanding code before writing.  PRIMM is also an acronym for the stages that it structures learning activities in: Predict, Run, Investigate, Modify, Make. 

**Predict** We start by presenting students with a prepared program, and the students try to predict what it will do. This works well as a think-pair-share or small groups activity, as discussion between students can enhance learning.

**Run** Then the students test their predictions by executing the program. Discussion can be used to help students verbalize the results and compare it with their predicted expectations.

**Investigate** The investigation stage is based on the Block Model, and can include activities like code tracing, explaining, annotating, debugging, etc.

**Modify** This stage is very similar to the Modify stage of Use - Modify - Create, and involves modifying the code they have just investigated to accomplish a related task. During this stage, the scaffolding support is gradually removed.

**Make** In this stage, learners are given a new problem to solve that uses the same ideas and skills introduced in earlier stages. 