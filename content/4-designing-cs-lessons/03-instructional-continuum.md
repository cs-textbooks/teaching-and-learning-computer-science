---
title: "CSSC Instructional Continuum"
pre: "3. "
weight: 30
date: 2018-08-24T10:53:26-05:00
---

Now let's turn our attention to thinking about approaches for delivering computer science lessons.  A handy way to consider the cognitive load is the the Computer Science Student-Centred Instructional Continuum[^1], which represents a range of potential instructional approaches:

![The Computer Science Student-Centred Instructional Continuum](/images/3.2.1.png)

The continuum ranges from highly scaffolded (right side) to no scaffolding (left side). Based on the learning theory from the prior chapter, we want to start our students with activities with more scaffolding, and gradually reduce that scaffolding.  Note that this applies for all new subjects - as we introduce them, we again want to have an appropriate level of scaffolding in place!

The continuum's activities are:

* __Copy Code__ - students are giving step-by-step instructions to follow. This can be part of a Live Coding exercise, a recorded tutorial, or a worksheet to copy.
* __Targeted Tasks__ - students are given a short task, i.e. completing a method, fixing buggy code, [transcribing music into a program](https://people.cs.ksu.edu/~nhbean/scratch#music), or Parsons problems.
* __Shared Coding__ - the teacher writes code, explaining their thought process (sometimes called demonstration coding, live coding, or apprenticeship). Students may be asked to write the same program at the same time.
* __Project Based__ - students are given a project with a goal - i.e. [adapt a play to be presented by sprites in Scratch](https://people.cs.ksu.edu/~nhbean/scratch#adaptation)
* __Inquiry Based__ - students consider a question and attempt to develop a solution, i.e. [use simulation to evaluate catapult designs](https://people.cs.ksu.edu/~nhbean/scratch#catapult)
* __Tinkering__ - completely student-led programming efforts.

This continuum can be helpful in evaluating existing curriculum materials; for example, 
most [Code.org](https://code.org/) lessons start with copy code activities using multimedia explanations of what that code does, and gradually shift to targeted tasks. Likewise, [Scratch](https://scratch.mit.edu/)'s built-in tutorials also start with copy code activities.  Once a student has developed comfort with this level of programming, you can introduce less tightly guided activities. 

[^1]: Jane Waite and Christine Liebe. 2021. Computer Science Student-Centered Instructional Continuum. In Proceedings of the 52nd ACM Technical Symposium on Computer Science Education (SIGCSE '21). Association for Computing Machinery, New York, NY, USA, 1246. DOI:https://doi-org.er.lib.k-state.edu/10.1145/3408877.3439591