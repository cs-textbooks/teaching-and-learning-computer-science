---
title: "The Block Model"
pre: "6. "
weight: 60
date: 2018-08-24T10:53:26-05:00
---

The Block Model is an educational model for describing how students come to understand a program as they _read_ it [^1]. It is expressed as a table that covers three dimensions in four levels.  Each cell represents one aspect of understanding. 

The three dimensions are the _text surface_ (the actual text of the program), the _program execution_ (the order in which the program commands happen, and how data is manipulated), and _goals_ (what the program is meant to do). Text surface and program execution are further grouped into _structure_ (the actual expression of code) and _function_ (its intended purpose).

The levels are _atoms_ (individual elements of the language, i.e. keywords and statements), _blocks_ (groupings of code lines that work together, i.e. a loop that sums all the values in a collection), _relations_ (the connections between the bocks), and _macro_ (the overall program).

<table>
  <tr>
    <th>Macro Structure</th>
    <td>Understanding the overall structure of the program</td>
    <td>Understanding the 'algorithm' of the program</td>
    <td>Understanding the goal/purpose of the program</td>
  </tr>
  <tr>
    <th>Relations</th>
    <td>References between blocks, eg. method calls, object creation, accessing data...</td>
    <td>sequence of method calls - 'object sequence diagrams'</td>
    <td>Understanding how subgoals relate to goals, how function is achieved by subfunctions</td>
  </tr>
  <tr>
    <th>Blocks</th>
    <td>'Regions of Interests' (ROI) that syntactically or semantically build a unit</td>
    <td>Operation of a block of code, a method, or a ROI (as a sequence of statements)</td>
    <td>Purpose of a block of code, possibly seen as a subgoal</td>
  </tr>
  <tr>
    <th>Atoms</th>
    <td>Language elements</td>
    <td>Operation of a statement</td>
    <td>Purpose of a statement</td>
  </tr>
  <tr>
    <th></th>
    <th>Text Surface</th>
    <th>Program Execution (data flow and control flow)</th>
    <th>Goals of the Program</th>
  </tr>
  <tr>
    <th>Duality</th>
    <th colspan="2" style="text-align: center">Structure</th>
    <th>Function</th>
</table>

The block model has a direct relationship with the epistemology of programming and the development of schema. Beginning programmers are still learning the atoms of a language (mastering these reflects achieving the preoperational stage). The first schema programmers develop are to reason about the patterns found in the blocks, and further schema help to reason about the relationships between these blocks (Concrete operational stage). Finally, students learn to see how all the pieces of a program come together at the macro level (Formal operational stage).

The block model can therefore help us reason about how our students are able to engage with a program (novices read programs bottom-up, reflecting the order of the table), while more experienced programmers often read top-down, sussing out the purpose of code by the names of functions, classes, etc. and by recognizing the purpose of blocks of code without needing to use code tracing.

[^1]: Carsten Schulte. 2008. Block Model: an educational model of program comprehension as a tool for a scholarly approach to teaching. In <i>Proceedings of the Fourth international Workshop on Computing Education Research</i> (<i>ICER '08</i>). Association for Computing Machinery, New York, NY, USA, 149–160. DOI:https://doi-org.er.lib.k-state.edu/10.1145/1404520.1404535