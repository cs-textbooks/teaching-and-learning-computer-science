---
title: "TIPP & SEE"
pre: "5. "
weight: 50
date: 2018-08-24T10:53:26-05:00
---

TIPP & SEE is an adaption of the Use &rarr; Modify &rarr; Create &rarr; learning progression developed specifically for working with Scratch[^1].  The name is an acronym for a multi-step process of interacting with Scratch projects. Scratch offers a website where programmers can publish and share their projects, and other programmers can [_remix_](https://en.scratch-wiki.info/wiki/Remix) them. 

Remixing in Scratch means taking an existing project and modifying it, often to build in more functionality or to change its aesthetics. This parallels the professional open-source community, where professional programs write and distribute programs online using sites like GitHub for others to use and modify. This also means that the Scratch website can serve as a rich source of exemplar programs to share with your students.

{{% notice tip %}}
Alternatively, you can provide a starting program for your students to remix that you authored yourself! 
{{% /notice %}}

![TIPP and SEE](/images/3.6.1.png)

TIPP stands for Title, Instructions, Purpose, and Play, and describes examining the surface level of a Scratch project - reading its title, instructions, trying to determine the purpose of the project, and then running it to see what it does. SEE stands for Sprites, Events, and Explore, and describes examining the actual code of the project.


[^1]: Jean Salac, Cathy Thomas, Chloe Butler, Ashley Sanchez, and Diana Franklin. 2020. TIPP&SEE: A Learning Strategy to Guide Students through Use - Modify Scratch Activities. Proceedings of the 51st ACM Technical Symposium on Computer Science Education. Association for Computing Machinery, New York, NY, USA, 79–85. DOI:https://doi.org/10.1145/3328778.3366821