---
title: "Embedded CT"
pre: "2. "
weight: 20
date: 2018-08-24T10:53:26-05:00
---

Before we delve into specific strategies for selecting and structuring lesson activities, let's first discuss more broadly what those activities should be. Many Computer Science curriculums focus on the development of algorithms and data structures (a fact reflected in the name of one of the best classic textbooks in the field, Nickolas Wirth's [Data Structures + Algorithms = Programs](https://en.wikipedia.org/wiki/Algorithms_%2B_Data_Structures_%3D_Programs)). We can see this in the CS Unplugged curriculums' focus on sorting algorithms and tree data structures, as well as many other curriculums.

While these topics are important, writing code to sort data or define data structures only appeals to a subset of our students. It also paints a somewhat unrealistic picture of what a computer science career would be for our students. The truth is, while it was once common practice, today very few programmers are actually writing sorting algorithms and data structures by hand. Instead, they are utilizing pre-built libraries that provide this functionality. While it is important to understand what these libraries are doing and how they structure data, it is not as important as many curriculums make it out to be.

Remember, computational thinking is all about _problem solving_, and the problems we want to solve come from all domains. Consider how real computer scientists and intellectual workers are using the tools of programming and computational thinking:
* A computer scientist working for an online media company revises their recommendation algorithm to do a better job of helping users find videos they will enjoy
* A social activist uses public databases and algorithms to reveal gerrymandering in a state's proposed redistricting
* An insurance actuary uses algorithms drawing on their company's historical claims data to determine appropriate insurance rates for teen drivers 
* A computational geneticist uses big databases and algorithms to search for patterns in genomes to discover the root cause of a disease
* A artist programs Raspberry Pis to cause elements of an interactive art exhibit to respond to patrons
* A computational physicist builds a simulation to test a theory of how our moon formed
* A DJ uses programming to help remix music from multiple tracks on the fly during a performance
* An advertiser uses profile data collected online to provide highly targeted ads to people most likely to be interested in the products being sold
 
Building or selecting lessons that reflect this breadth of the use and impact of computer science can help students see that they can use the tools of computer science to support their interests. Research has shown that this approach also appeals to students who would never have thought computer science was something they would be interested in.

{{% notice tip %}}
If you are a K-12 teacher, you have a unique opportunity to incorporate computer science activities into the other subjects you teach. This has the benefit of both adding depth to your CS lessons and also showing students how CS is normally used to solve problems in other domains!
{{% /notice %}}