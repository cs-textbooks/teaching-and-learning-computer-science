---
title: "Use - Modify - Create"
pre: "4. "
weight: 40
date: 2018-08-24T10:53:26-05:00
---
The _Use &rarr; Modify &rarr; Create_ learning progression is a good example of a lesson design strategy that incorporates activities that progressively move from left to right on the instructional continuum. Students are introduced to an exemplar program that does something similar to the ultimate challenge you have in mind for them.  They run the program and learn how to **use** it effectively.  Then they are tasked with **modify**ing the program in a way that allows them to tackle a different (but similar problem). Finally, they use what they hae learned to **create** an entirely new program[^1].

![The Use-Modify-Create Progression](/images/3.5.1.png)

In addition to providing a gradual removal of scaffolding when teaching computer science, the Use-Modify-Create approach also encourages students to develop a growing sense of ownership over the code they author. This progression can be repeated to introduce new syntax, algorithms, or data models. Later projects can also be designed to challenge students to combine the approaches they have previously learned separately.  

[^1]: Nicholas Lytle, Veronica Cateté, Danielle Boulden, Yihuan Dong, Jennifer Houchins, Alexandra Milliken, Amy Isvik, Dolly Bounajim, Eric Wiebe, and Tiffany Barnes. 2019. Use, Modify, Create: Comparing Computational Thinking Lesson Progressions for STEM Classes. In Proceedings of the 2019 ACM Conference on Innovation and Technology in Computer Science Education (ITiCSE '19). Association for Computing Machinery, New York, NY, USA, 395–401. DOI:https://doi.org/10.1145/3304221.3319786