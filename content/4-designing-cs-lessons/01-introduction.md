---
title: "Introduction"
pre: "1. "
weight: 10
date: 2018-08-24T10:53:26-05:00
---

We've covered what computer science is, how it is learned, and have seen some teaching approaches and tools to help manage cognitive load.  Building on that knowledge, in this chapter we'll look at how to design or modify computer science lessons for our students.