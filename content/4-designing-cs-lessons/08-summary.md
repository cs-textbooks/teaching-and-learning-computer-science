---
title: "Summary"
pre: "8. "
weight: 80
date: 2018-08-24T10:53:26-05:00
---

In this chapter we explored pedagogical techniques for evaluating and developing computer science lessons.  We discussed embedding our lessons in the problem spaces of other disciplines, and how this can help students understand better how computer science fits into the world.  

We also saw how the Student-Centered Computer Science Instructional Continuum can be used to help think about scaffolding to control cognitive load in our lessons. Then we saw a number of instructional models for developing CS lessons, including Use-Modify-Create, TIPP & SEE, the Block Model, and PRIMM.