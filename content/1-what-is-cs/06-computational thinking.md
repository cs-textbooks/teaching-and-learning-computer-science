---
title: "Computational Thinking"
pre: "6. "
weight: 60
date: 2018-08-24T10:53:26-05:00
---

The term "Computational Thinking" has recently caught on as a way to describe how we can approach problem-solving using computation. While there is some debate on exactly what constitutes computational thinking, I like the definition:

> Computational thinking is the thought processes involved in formulating problems and their solutions so that the solutions are represented in a form that can effectively be carried out by an information-processing agent.

In other words, computational thinking is about reformulating problems. You've already done this in your own education.  Remember word problems like:

> Two trains leave a station, one a passenger train traveling east at 8 miles per hour, and one a freight train traveling west at 12 miles per hour. The two stations are 521 miles apart.  Assuming the two trains are traveling on parallel tracks, how much time will elapse before they pass each other?

To solve this, you probably would reformat it into algebraic formulas:

```math
$$
  position_1(t_n) = velocity_1 * t_n
$$
$$
  position_2(t_n) = velocity_2 * t_n
$$ 
$$
  distance(t_n) = position_1(t_n) + position_2(t_n)
$$
```

After which you could combine the formulas:

```math
$$
  distance(t_n) = velocity_1 * t_n + velocity_2 * t_n
$$
```

Substitute known values:

```math
$$ 
  560 = 8 * t_n + 12 * t_n
$$
```

And solve for {{< math >}} $t_n$ {{< /math >}} :

```math
$$
  560 = 20 * t_n
$$
$$
  560/20 = t_n
$$
$$
  t_n = 28 
$$
```

In this example, _**you** are the information-processing agent_.  You reformulated the problem into a form you knew how to solve (algebraic expressions). 

But when we talk about computational thinking, the information-processing agent we are typically referring to is a digital computer. We need to structure the problem in a form _it_ can process - a _program_. Thus, computational thinking is really about taking a real-world problems and reformulating them as programs that solve them.

Note that we need _two separate but related skills_ to do this - we must be able to come up with an approach to find a solution, and we must know enough programming to express it.