---
title: "Introduction"
pre: "1. "
weight: 10
date: 2018-08-24T10:53:26-05:00
---

If you are reading this, you are most likely getting ready to teach a course or unit on computer science. Congratulations! 

But before we delve into _how_ to best teach the subject, it is important to first understand _what_ computer science is, and _how_ we learn it. This chapter will address the first of those topics - what computer science actually _is_ as a discipline, and why we want to teach it to our students.

