---
title: "Computing Hardware"
pre: "3. "
weight: 30
date: 2018-08-24T10:53:26-05:00
---

Computer hardware actually has a long and storied history. Early computers included devices like the [Antikythera mechanism](https://en.wikipedia.org/wiki/Antikythera_mechanism), a clockwork device for calculating the positions of the planets built in first or second-century BC in Greece and mechanical calculators incorporating the [Leibniz wheel](https://en.wikipedia.org/wiki/Leibniz_wheel). These early devices can be classified as _fixed-program_ computers, as they can only carry out specific kinds of activities as determined by their structure.

[Charles Babbage](https://en.wikipedia.org/wiki/Charles_Babbage) invented two computing devices, the [Difference Engine](https://en.wikipedia.org/wiki/Difference_engine) and the [Analytical Engine](https://en.wikipedia.org/wiki/Analytical_Engine), both mechanical computers that carried out calculations, that were notable for their size and complexity.  The Analytical Engine also marked the first _programmable_ computer design - one that could be easily reconfigured to carry out different kinds of operations. In describing the function of this computer, [Ada Lovelace](https://en.wikipedia.org/wiki/Ada_Lovelace) proposed using the computer to compose original music, marking the first time computers were considered for broader use than computations.  

{{% notice info %}}
Babbage's Difference Engine No. 2 was designed in 1840, but never built until the modern era.  This video discusses that effort spearheaded by the Science Museum of London.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0anIyVGeWOI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
{{% /notice %}}

The [Z3](https://en.wikipedia.org/wiki/Z3_(computer)) and [ENIAC](https://en.wikipedia.org/wiki/ENIAC) were some of the earliest _electronic_ computer devices. They used transistors (switches that could be on or off) to represent data.  With only two states, these computers were designed to operate with _binary_ math - 0's and 1's. This was a major departure from earlier approaches which operated in the more familiar base 10 (Babbage's engines represented digits by wheels with 10 faces). Both were programmable: the Z3 read its program from a punched tape, while the ENIAC could be programmed through the use of a plugboard similar to those used by phone operators. The Z3 was considered non-vital by the German military, and was later destroyed in bombing.  In contrast, the ENIAC was used to carry out calculations to support American World War II efforts (including carrying out calculations for the atom bomb), and during this time was exclusively programmed by women mathematicians, including [Kay McNulty](https://en.wikipedia.org/wiki/Kathleen_Antonelli), [Betty Jennings](https://en.wikipedia.org/wiki/Jean_Bartik), [Betty Snyder](https://en.wikipedia.org/wiki/Betty_Holberton), [Marylyn Wescoff](https://en.wikipedia.org/wiki/Marlyn_Meltzer), [Fran Bilas](https://en.wikipedia.org/wiki/Frances_Spence), and [Ruth Licherman](https://en.wikipedia.org/wiki/Ruth_Teitelbaum). Their story is discussed in the documentary [Top Secret Rosies: The Female "Computers" of WWWII](https://en.wikipedia.org/wiki/Top_Secret_Rosies:_The_Female_%22Computers%22_of_WWII).

![Programmers Betty Jean Jennings (left) and Fran Bilas (right) operate the main control panel of ENIAC](/images/ENIAC.jpg)

The next major step forward was _stored program_ computers - computers that had a limited number of instructions they could carry out that were stored in the computer's memory along with the data on which those programs would operate. These were originally proposed by [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing) with his [Turing Machine](https://en.wikipedia.org/wiki/Turing_machine). The physical design on which modern computers are all based was popularized by [John von Neumann](https://en.wikipedia.org/wiki/John_von_Neumann) and is known as the [Von Neumann Architecture](https://en.wikipedia.org/wiki/Von_Neumann_architecture).

Thus, modern computers share certain characteristics:
1. They represent all information in a binary form
2. They can carry out a handful of operations on this binary information 
3. These instructions can be triggered by _programs_, sequences of these operations
4. Both the programs and the data they operate on are stored in the memory of the computer

But hardware is only part of the picture. The programs that run on them are just as important. We'll turn to these next.