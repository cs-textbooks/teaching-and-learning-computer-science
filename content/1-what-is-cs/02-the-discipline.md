---
title: "The Discipline"
pre: "2. "
weight: 20
date: 2018-08-24T10:53:26-05:00
---

Broadly speaking, Computer Science is: 

>  The branch of knowledge concerned with the construction, programming, operation, and use of computers. (Oxford English Dictionary)

As this definition suggests, Computer Science lies at the intersection of computer hardware, computational techniques, and problem-solving approaches (computational thinking). 

It is a relatively _young_ discipline, only emerging as a distinct academic area in the 1950's and 60's.  Prior to this point, people building and working with computers were drawn from related disciplines: mathematics, engineering, physics, linguistics, and philosophy.

In this chapter, we'll examine each briefly before discussing how people engage with the field.