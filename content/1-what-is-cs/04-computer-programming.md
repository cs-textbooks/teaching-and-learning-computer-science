---
title: "Computer Programming"
pre: "4. "
weight: 40
date: 2018-08-24T10:53:26-05:00
---

As described in the previous section, modern computers have a limited instruction set that largely concerns moving binary data between memory and CPU registers, and performing mathematical operations on data in those registers.  Programs are expressed in 'assembly code' or the binary equivalent 'machine code' and are essentially sequences of those commands.  For example, a program to print the line "Hello World" looks like:

```asm
global    _start

          section   .text
_start:   mov       rax, 1                  ; system call for write
          mov       rdi, 1                  ; file handle 1 is stdout
          mov       rsi, message            ; address of string to output
          mov       rdx, 13                 ; number of bytes
          syscall                           ; invoke operating system to do the write
          mov       rax, 60                 ; system call for exit
          xor       rdi, rdi                ; exit code 0
          syscall                           ; invoke operating system to exit

          section   .data
message:  db        "Hello, World", 10      ; note the newline at the end
```

Writing programs in assembly code is a considerable challenge.  Thankfully, visionary programmers like [Grace Hopper](https://en.wikipedia.org/wiki/Grace_Hopper), [Betty Holberton](https://en.wikipedia.org/wiki/Betty_Holberton), and [Adele Mildred Koss](https://en.wikipedia.org/wiki/Milly_Koss) realized they could use programs to write portions of other programs. Grace Hopper took this to the next logical step, writing a program that could translate a more human-friendly language into machine code, known as FLOW-MATIC. 

In her later contributions to COBOL, Grace Hopper successfully advocated for expressing program instructions in English. This concept of compiling a human-readable programming language (a higher-order programming language) into machine code is the basis of all modern programming languages: C/C++, Java, Python, JavaScript, C#, etc.

Thus, the same program as above expressed in Python becomes:

```python
print("Hello, World")
```

Much easier to understand!

Programming languages have continued to evolve, introducing new syntax and organizational approaches intended to make it easier to write programs, reduce errors, improve security, and encourage good programming practices.  

One advance in programming languages that is of especial interest to teachers are _block-based_ programming languages like [Scratch](https://en.wikipedia.org/wiki/Scratch_(programming_language)) and [Blockly](https://en.wikipedia.org/wiki/Blockly). Instead of typing the program source code, in these languages you assemble the code by connecting graphical blocks. This approach avoids the possibility of an entire family of syntax errors, as the blocks will only snap together in ways that the compiler can interpret. As such, it makes learning to program more approachable for novices. 

The same program as above in Scratch is:

![Hello World in Scratch](/images/1.4.1.png)

Block-based programming is a great starting place for learning programming and computer science. But writing code by dragging and dropping blocks is also slower than typing, and eventually most programmers will transition to learning a text-based language.

{{% notice info %}}
**What is in a Name?**
You may have noticed that the practice of writing programs has multiple names, for example: programming, coding, and hacking. Likewise, programs themselves have multiple names: programs, applications, apps, and software.  You may wonder if there is a difference, and what _should_ we call them?

When we write a program using a higher-order programming language, what we have written is the _source code_. Thus, _programming_ or _coding_ make a lot of sense.  The term _programming_ carries a slightly more serious connotation, while _coding_ seems lighter and more fun - which is probably why it has been adopted by the _Coding movement_ which advocates for getting kids involved in programming. Nonetheless, the two terms can (and are) used interchangeably.

_Hacking_ is a bit more problematic.  It suggests a more free-form approach to programming, such as ignoring guidelines on how to structure code to be readable to others.  Moreover, the term also encompasses the exploitation of computer technology for committing crimes. So it is probably best to avoid using it to refer to the practice of writing programs.

Similarly, a compiled program is an application (app for short).  More broadly, an application is a program intended to be used by a person, while programs and software may instead be intended to be run by other programs or directly control a mechanical system (i.e. a robot vacuum cleaner).  As programs are stored in memory in a stored-program computer rather than being built into the computer's physical architecture (the _hardware_), we can also call them _software_.
{{% /notice %}}