---
title: "Summary"
pre: "7. "
weight: 70
date: 2018-08-24T10:53:26-05:00
---

In this chapter we discussed what computer science is as a discipline and explored a bit of its history and foundations.  We also discussed computational thinking, a problem-solving approach that leverages the tools of computer science - computers and programs - to solve problems. We briefly covered how computational thinking is transforming how people solve problems in their disciplines, which helps explain why it is increasingly necessary to teach it to our students.  

In the next chapter, we will look at the science behind how people actually learn computer science and computational thinking.