---
title: "Intellectual Workers"
pre: "5. "
weight: 50
date: 2018-08-24T10:53:26-05:00
---

In his landmark address now known as [The Mother of All Demos](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi8v8aG3-v2AhVROH0KHaB3BYwQFnoECBwQAw&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FThe_Mother_of_All_Demos&usg=AOvVaw23eBZS6lRPQc-Q4EVIHKW6), the computer scientist Douglas Engelbart introduced an early computer operating system developed at SRI. This system and demo introduced ideas like the computer mouse, graphics, hypertext, hyperlinking, collaborative real-time editing, videoconferencing, and screen sharing. These ideas became the basis and inspiration for both the Windows and Macintosh operating systems, as well as the World-Wide Web.

{{% notice info %}}
SRI released the full recording of Douglas Engelbart's demo to YouTube, and the playlist is embedded below.  Be warned - the full presentation is 1 hour and 45 minutes (which helps explain the nickname "Mother of all Demos")

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL76DBC8D6718B8FD3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
{{% /notice %}}

In proposing these technologies, Engelbart suggested that they would support the development of a new class of "Intellectual Worker," a new class of worker that would use computers to communicate, structure, store, and retrieve data and use computer technology to augment their natural problem-solving capabilities. The introduction of the personal computer in the 1980's accelerated the development of this class of worker, who primarily relied upon programs built to support various efforts (i.e. a word processor for writing documents, an email client for sending emails). Thus, an intellectual worker was primarily a _consumer_ of programs written by computer scientists.

However, as computing hardware became more available and widely used increasingly intellectual workers began to author their _own_ programs to meet the specific needs of their work. This exploration of programming within specific domains has transformed the way we approach problems in multiple fields:

> We have witnessed the influence of computational thinking on other disciplines. For example, machine learning has transformed statistics. Statistical learning is being used for problems on a scale, in terms of both data size and dimension, unimaginable only a few years ago. […] Computer science’s contribution to biology goes beyond the ability to search through vast amounts of sequence data looking for patterns. The hope is that data structures and algorithms – our computational abstractions and methods – can represent the structure of proteins in ways that elucidate their function. Computational biology is changing the way biologists think. Similarly, computational game theory is changing the way economist think; nanocomputing, the way chemists think; and quantum computing, the way physicists think. (Jeanette Wing, 2006, p. 34)

In other words, intellectual workers are increasingly using the _tools_ of computer science, not just the _products_.  This realization has driven the call for teaching computer science in K-12 education - as today's students will increasingly need to be able to use computational thinking and programming in their future work, _even if they aren't going to be a computer scientist_.