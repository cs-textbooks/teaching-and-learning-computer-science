---
title: "Cognitive Load"
pre: "4. "
weight: 40
date: 2018-08-24T10:53:26-05:00
---

The theory of _cognitive load_ suggests that we can only hold five to nine pieces of novel information in our _working memory_ at a time. This working memory is the information at the "tip of your mind."  Have you ever walked upstairs to get something and forgotten what it was by the time you got there? This is because it passed out of your working memory (likely to make room for something new that distracted you on the way).

If this is the case, how do we get anything done?  We create mental structures that allow us to store multiple pieces of information in _schemas_, essentially, patterns of information that can help us work with it, as a schema populated with information is treated as a single chunk by working memory. These schemas can help us organize information and also automate behaviors.

Remember learning to drive a car?  How you had to pay attention to so many things?  What was on the road ahead of you, what was behind you, next to you.  What your control panel was displaying?  How fast you were going?  What the speed limit was?  Likely you found it difficult to keep all of that in focus - but as you developed schema to help with it, driving became much easier.  Possibly to the point that sometimes you don't even remember how you got to your destination!

Creating new schema is essentially what the process of _accommodation_ is.  And once those schemas exist, learning new information that maps to them is the process of _assimilation_. For example, once you've learned what mammals are and several examples of mammals, learning about a new one is mostly a process of determining how it compares to the ones you already know about.

Understanding cognitive load can help us teach better, because we can take steps to reduce the number of pieces of novel ideas our students need to grapple with until they can develop their schema.  Building scaffolding into early lessons and then gradually removing it is a key strategy.  Also, cognitive load can help us better understand the challenges faced by students with ADD - in this condition, the brain has trouble determining what information should be held in working memory, and often displaces it with other sensory information.