---
title: "Mindsets"
pre: "5. "
weight: 50
date: 2018-08-24T10:53:26-05:00
---

Carol Dweck is a researcher who has been developing a theory on _Mindsets_, which are individual beliefs we each possess about what we are capable of learning.  She describes her research in the following Ted Talk:

<iframe width="697" height="522" src="https://www.youtube.com/embed/_X0mgOOSpLU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Essentially, her work identifies two common mindsets - a _fixed mindset_, which suggests that you have an innate capacity for learning a subject that cannot be exceeded, or a _growth mindset_ which suggests that with practice and effort, you can continue to learn. 

These mindsets influence how we cope with the cognitive disequilibrium from Piaget's Genetic Epistemology. Remember, this disequilibrium is necessary to trigger accommodation - the process of building new mental structures. But it is also uncomfortable and frustrating - we often describe it as "hitting a wall" or "banging my head against a problem."

Students with a fixed mindset encountering this disequilibrium assume they've reached the limit of what they can learn in the field. As you might expect, they don't see the point of continuing to try.  Instead, they look for other ways to relieve the discomfort that the disequilibrium is creating. They may act out, withdraw from active participation, drop the course, or even engage in cheating.

In contrast, students with a growth mindset believe that with more effort, they can persevere and learn what they are struggling with.  Thus, they continue to try.  And in doing so, they will eventually resolve that disequilibrium by the process of accommodation - building those new mental structures that allow them to reason about programming and computational thinking in new ways.

With this in mind, fostering a growth mindset should _always_ be a goal while we teach computer science.  Also, be aware that our mindsets are discipline-specific: a student can have a growth mindset in one area (like science) and a fixed mindset in another (i.e. mathematics).