---
title: "Stage Theory"
pre: "7. "
weight: 70
date: 2018-08-24T10:53:26-05:00
---

In addition to the mechanisms of accommodation and assimilation Jean Piaget outlined in his Genetic Epistemology theories, Piaget identified four stages children progress through as they learn to reason more abstractly.  Those stages are:

* **Sensorimotor** - where the learner uses their senses to interact with their surroundings.  This is the hallmark of babies and toddlers who gaze wide-eyed at, touch, and taste the objects in their surroundings.
* **Preoperational** - in this stage the learner begins to think _symbolically_, using words and pictures to represent objects and actions.
* **Concrete Operational** - In this stage, the learner begins to think logically, but only about concrete events.  Inductive logic - the ability to reason from specific information to a general principle also appears.
* **Formal Operational** - This final stage marks the ability to grasp and work with abstractions, and is marked by hypothetico-deductive reasoning (i.e. formulating and testing hypotheses)

## Neo-Piagetian Theory
While Piaget focused his study on children, many of the researchers who followed him also looked at how adults learn.  Their findings suggest that _all_ learners progress through the four stages with _any_ new learning.  That is, when you begin to learn a novel concept or skill, you are building the cognitive structures necessary to support it, and that your progress through this process corresponds to these four stages.  Moreover, they have found that the divisions between stages are not rigid and clearly delineated; learners can exist in multiple stages at once (which they call the _overlapping waves model_).

![The Overlapping Waves Model](/images/2.6.1.png)

Neo-Piagetians have gone on to create domain-specific theories expounding on these ideas.  We'll take a look at one grounded in the discipline of programming next section. 