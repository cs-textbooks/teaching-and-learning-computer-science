---
title: "Zone of Proximal Development"
pre: "6. "
weight: 60
date: 2018-08-24T10:53:26-05:00
---

Another theory that often is referenced with teaching and curriculum development is [Lev Vigotsky](https://en.wikipedia.org/wiki/Lev_Vygotsky)'s [Zone of Proximal Development](https://en.wikipedia.org/wiki/Zone_of_proximal_development). This zone represents the distance between what a student can do unaided, what they can do with assistance, and what they are incapable of doing.

![Zone of Proximal Development](/images/2.5.2.png)

As a student learns, the central zone (what the student can do) grows, reflecting the development of new cognitive structures in their mind that support the knowledge and skills they need to use.

As that central zone grows, the zone of proximal development is likewise pushed outward. From a genetic epistemology standpoint, this zone represents the "right" amount of challenge to create enough disequilibrium to trigger accommodation, but not so much to overwhelm the learner. 

Keeping this zone in mind when developing or selecting curriculum materials is especially important - you don't want your tasks to fall too far into the center zone (representing boring make-work), or in the outer zone (too challenging for the student to even know where to start).  You want to keep the tasks firmly in the students' zone of proximal development. We'll discuss strategies for this in the next chapter. But before we do, we'll return to Jean Piaget and some of his _other_ theories.
