---
title: "Genetic Epistemology"
pre: "3. "
weight: 30
date: 2018-08-24T10:53:26-05:00
---

You have probably heard of [Constructivism](https://en.wikipedia.org/wiki/Constructivism_(philosophy_of_education)), a theory that states as we learn we are 'constructing' a mental model through which we understand the world.  The foundations of this philosophy arise from the work of Jean Piaget, a biologist and psychologist who performed some of the earliest studies on knowledge acquisition in children.

Of especial interest to us is his theory of _genetic epistemology_.  Epistemology is the study of human knowledge, and genetic in this sense refers to _origins_ i.e. the _genesis_, so his theory concerns _how knowledge is created by humans_.

Piaget's genetic epistemology was inspired by studies he conducted of snails of the genus Lymnea native to the lakes of his home, Switzerland.  He was able to establish that what had previously been considered different species of snails based on the shape of their shells were actually one species.  He showed that when the snails of one lake were placed in a different lake, the way their shells grew changed to match those of the snails living in the second lake.  In other words, the traits the snails displayed altered in response to their environment.

![Examples of Lymnea](/images/2.3.2.jpg)

Piaget suspected a similar mechanism was at work in human learning.  He proposed that the human brain, much like the bodies of the snails, sought to exist in equilibrium with its environment.  For the brain, this environment meant the ideas it was exposed to.  He proposed two different mechanisms for learning, _accommodation_ and _assimilation_, which related to how structures of knowledge were formed in the brain.  

Assimilation referred to the process of adding new knowledge into existing mental structures.  For example, once you know of and understand colors, you might encounter a new one - say periwinkle, which falls between blue and violet.  Since you already know blue and violet, adding the knowledge of periwinkle is a straightforward task for the brain of assigning it to a slot between the two in the mental structure of 'colors'.  

In contrast, accommodation refers to the process by which knowledge _for which you have no mental structures to represent_ are learned. This process involves _building_ these mental structures, and is far more work.  Modern cognitive science equates this process to the formation and reinforcement of new connections between neurons - a significant biological investment. Thus, triggering accommodation requires significant stimulus, in the form of wrestling with concepts that are currently beyond your grasp - a struggle that creates _disequilibrium_ for your brain, eventually leading to it creating the new structure to accommodate the new knowledge.

This is the basis of the 'eureka' moment, when a difficult concept has finally become clear.  No doubt you have experienced this in a subject such as mathematics or programming, where a skill or idea you've been struggling with suddenly snaps into place, and becomes far easier to work with.  This is also why your math and programming courses put so much emphasis on homework - this work helps create the disequilibrium you need to trigger accommodation.  

This understanding of the process of knowledge acquisition and accommodation have some important implications, as well as tying into other theories of learning.  Let's look at those next.