---
title: "Notional Machines"
pre: "9. "
weight: 90
date: 2018-08-24T10:53:26-05:00
---

Going back to the previous chapter, we discussed both computing hardware and programming languages.  Remember, modern computers have very limited instruction sets. But a programming language can provide a single command that translates into a _lot_ of individual instructions in machine code. 

Thus, when writing programs, we need to have some knowledge of both.  But the good news is we don't need _exhaustive_ knowledge - just enough to understand the programs we are writing. Computer scientists often call this combined functionality the _notional machine_, an idealized computer that represents the combination of features from the hardware and programming language that together establish how a program running on that machine will behave.  In other words, it is a simplified model of how computation is carried out. 

The concept of notional machines was originally developed to help _teach_ computer science. But when we adopt a constuctivist standpoint, we can also argue that in learning to program, each student is actually constructing their **own** notional machine, which incorporates everything they understand about the hardware and programming language they are learning.  This concept of an _internalized notional machine_ is very helpful, as a student misconception is essentially a flaw in that internalized notional machine. Because of this misalignment between their internalized notional machine and the real notional machine, a program that they write may not behave as they expect it to.

The process of developing programming skill is therefore one of developing a robust and accurate internalized notional machine. The skills of computational thinking then _use_ this internalized notional machine to determine how to solve problems, as well as create the programs to embody those solutions.


