---
title: "Introduction"
pre: "1. "
weight: 10
date: 2018-08-24T10:53:26-05:00
---

Now that you understand a bit of what computer science _is_ as a discipline, we will turn our attention to how it is _learned_. Understanding the learning process is key to effective teaching, especially in how it is grounded in a domain (like computer science).  Once you grasp how learning occurs:
1. You will be more aware of what challenges your students are facing
2. You will have a better grasp of how to support their learning
3. You can better evaluate or develop pedagogically-grounded lessons for your teaching

As we go through this chapter, remember that computer science is all about solving problems with computers through programming and computational thinking.
