---
title: "Summary"
pre: "10. "
weight: 100
date: 2018-08-24T10:53:26-05:00
---

In this chapter, we explored theories of learning and how learning computer science fits into them.  In particular, we saw that learning computer science and programming _creates new structures in the mind_ corresponding to physical changes in the connections between brain neurons. Piaget's genetic epistemology theory tells us the process of building these structures requires a certain amount of _cognitive disequilibrium_ - grappling with the subject material and trying to make sense of it.  

Carol Dweck's theory of mindsets helps us understand how students respond to this disequilibrium. Ideally our students adopt a _growth mindset_ which encourages them to continue to work at learning the material, eventually building those mental structures. Vigotksy's _zone of proximal development_ helps define how much disequilibrim we need to create, and provides some guidance on how to control that. 

Pigaet's stage theory, as elaborated in _the developmental epistemology of programming_, gives us a way to reason about where students' development is. And the neo-Piagitian overlapping waves model helps us understand that development is specific to individual concepts within the domain. And finally, _internalized notional machines_ are a way of understanding exactly what some of those mental structures our students are building are - a mental model of how the computer and programming language work together that helps them design and write programs.