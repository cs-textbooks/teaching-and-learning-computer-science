---
title: "Natural Born Programmers"
pre: "2. "
weight: 20
date: 2018-08-24T10:53:26-05:00
---

There is a prevalent myth that some people are "natural born programmers" to whom programming comes easily. This is a _dangerous_ idea, as in accepting it, you are _also_ accepting anyone who struggles learning programming is _not meant to be a programmer_.  

{{% notice warning %}}
This idea is _especially_ dangerous idea for a teacher, as accepting it **will cause bias** in how you interact with your students. Research has consistently shown that this kind of unconscious bias on part of teachers subtly but effectively influences students' interest and effort in learning computer science. We'll explore this and related issues more in a later chapter. 
{{% /notice %}}

The truth is that in learning to _program_, we are learning to solve problems _in a way that can be performed by a computing machine_. As we discussed in the previous chapter, this must be expressed in a programming language that has a limited and specific set of operations it can carry out. It has no ability to _interpret_ our intent - only the ability to carry out instructions _exactly as written_ and only if these are written in a language it knows.

As a way of exploring these limitations, see how giving instructions to a "computational agent" goes for the Darnit family:

<iframe width="560" height="315" src="https://www.youtube.com/embed/FN2RM-CHkuI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The point of this exercise is to understand the _exactitude_ or _precision_ with which programs must be written. But it also reveals just how different people are from computers. The simple truth is that to become good programmers, we must _learn_ to write programs by developing an understanding of how the computers work, as well as how to express instructions in a form they can use. 

The rest of this chapter is devoted to understanding that learning process.